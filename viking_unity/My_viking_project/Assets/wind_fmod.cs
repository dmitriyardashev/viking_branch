﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind_fmod : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string windEvent;
    FMOD.Studio.EventInstance windinstance;
    // Start is called before the first frame update
    void Start()
    {
        //FMODUnity.RuntimeManager.PlayOneShot(windEvent); 
        windinstance = FMODUnity.RuntimeManager.CreateInstance(windEvent);
        windinstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
