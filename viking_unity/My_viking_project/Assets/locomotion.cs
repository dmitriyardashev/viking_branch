﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class locomotion : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string walkingEvent;
    public GameObject feet;
    vThirdPersonInput Magnitude;




    // Start is called before the first frame update
    void Start()
    {
        Magnitude = GetComponent<vThirdPersonInput>();
    }

 
    void walking()
    {
        if (Magnitude.cc.inputMagnitude > 0.1)
        {
            FMODUnity.RuntimeManager.PlayOneShot(walkingEvent, feet.transform.position);
        }
        

    }
}
